/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Injectable } from '@angular/core';
import {
  GridFailure,
  FailureBranch,
  FailureClassification,
  FailureState,
  FailureRadius,
  FailureExpectedReason,
  FailureStation,
  FailureHousenumber,
  FailureAddress,
  DistributionGroup,
  PublicationChannel,
  Polygon,
} from '@grid-failure-information-app/shared/models';

@Injectable()
export class GridFailureService {
  static gridAdapter(response: any): Array<GridFailure> {
    return response.map(responseItem => new GridFailure(responseItem));
  }

  static itemAdapter(responseItem: any): GridFailure {
    return new GridFailure(responseItem);
  }

  static branchListAdapter(response: any): Array<FailureBranch> {
    return response.map(responseItem => new FailureBranch(responseItem));
  }

  static classificationListAdapter(response: any): Array<FailureClassification> {
    return response.map(responseItem => new FailureClassification(responseItem));
  }

  static stateListAdapter(response: any): Array<FailureState> {
    return response.map(responseItem => new FailureState(responseItem));
  }

  static radiusListAdapter(response: any): Array<FailureRadius> {
    return response.map(responseItem => new FailureRadius(responseItem));
  }

  static expectedReasonListAdapter(response: any): Array<FailureExpectedReason> {
    return response.map(responseItem => new FailureExpectedReason(responseItem));
  }

  static stationAdapter(response: any): FailureStation {
    return new FailureStation(response);
  }

  static stationListAdapter(response: any): Array<FailureStation> {
    return response.map(responseItem => new FailureStation(responseItem));
  }

  static polygonAdapter(response: any): Array<[number, number]> {
    return response;
  }

  static housenumberListAdapter(response: any): Array<FailureHousenumber> {
    return response.map(responseItem => new FailureHousenumber(responseItem));
  }

  static addressItemListAdapter(response: Array<string>): Array<string> {
    return !!response && response.length > 0 ? response : [];
  }

  static addressAdapter(responseItem: any): FailureAddress {
    return new FailureAddress(responseItem);
  }

  static distributionGroupListAdapter(response: any): Array<DistributionGroup> {
    return response.map(responseItem => new DistributionGroup(responseItem));
  }

  static distributionGroupAdapter(response: any): DistributionGroup {
    return new DistributionGroup(response);
  }

  static publicationChannelListAdapter(response: any): Array<PublicationChannel> {
    return response.map(responseItem => new PublicationChannel(responseItem));
  }

  static publicationChannelAdapter(response: any): PublicationChannel {
    return new PublicationChannel(response);
  }

  static reminderAdapter(response: any): boolean {
    return response;
  }
}
