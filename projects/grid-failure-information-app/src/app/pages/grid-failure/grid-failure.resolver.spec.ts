/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { GridFailuresResolver } from '@grid-failure-information-app/pages/grid-failure/grid-failure.resolver';

describe('GridFailuresResolver', () => {
  let component: GridFailuresResolver;
  let sandbox: any;
  let detailSandbox: any;

  beforeEach(() => {
    sandbox = {
      clear() {},
      loadGridFailures() {},
      setGridFailureId() {},
    } as any;
    detailSandbox = {
      setGridFailureId(id: string) {},
      loadGridFailure() {},
      loadGridFailureVersions() {},
      loadGridFailureBranches() {},
      loadGridFailureClassifications() {},
      loadGridFailureStates() {},
      loadGridFailureRadii() {},
      loadStations() {},
      loadGridFailureCommunities() {},
      loadGridFailureStations() {},
      loadDistributionGroups() {},
      loadGridFailureDistributionGroups() {},
      loadGridFailurePublicationChannels() {},
      loadGridFailureExpectedReasons() {},
    } as any;
  });

  beforeEach(() => {
    component = new GridFailuresResolver(sandbox, detailSandbox);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call loadGridFailures', () => {
    const spy = spyOn(sandbox, 'loadGridFailures');
    let ar: any = { params: { gridFailureId: undefined } };
    component.resolve(ar);
    expect(spy).toHaveBeenCalled();
  });

  it('should call loadGridFailure', () => {
    const spy = spyOn(detailSandbox, 'loadGridFailure');
    const spy2 = spyOn(detailSandbox, 'loadGridFailureVersions');
    let ar: any = { params: { gridFailureId: '6432a9c9-0384-44af-9bb8-34f2878d7b49' } };
    component.resolve(ar);
    expect(spy).toHaveBeenCalled();
    expect(spy2).toHaveBeenCalled();
  });
});
