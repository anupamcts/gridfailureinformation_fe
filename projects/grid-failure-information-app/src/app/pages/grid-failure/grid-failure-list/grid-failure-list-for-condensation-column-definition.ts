/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { DateTimeCellRendererComponent } from '@grid-failure-information-app/shared/components/cell-renderer/date-time-cell-renderer/date-time-cell-renderer.component';
import { IconCellRendererComponent } from '@grid-failure-information-app/shared/components/cell-renderer/icon-cell-renderer/icon-cell-renderer.component';
import { dateTimeComparator } from '@grid-failure-information-app/shared/utility';

export const GRID_FAILURE_FOR_CONDENSATION_COLDEF = [
  {
    field: 'failureClassification',
    headerName: 'GridFailure.FailureClassification',
    sortable: true,
    suppressMovable: true,
    filter: 'setFilterComponent',
  },
  {
    field: 'responsibility',
    headerName: 'GridFailure.Responsibility',
    sortable: true,
    suppressMovable: true,
    filter: 'setFilterComponent',
  },
  {
    field: 'description',
    colId: 'description',
    headerName: 'GridFailure.Description',
    maxWidth: 300,
    sortable: true,
    suppressMovable: true,
    filter: 'setFilterComponent',
  },
  {
    field: 'statusIntern',
    headerName: 'GridFailure.StatusIntern',
    sortable: true,
    suppressMovable: true,
    filter: 'setFilterComponent',
  },
  {
    field: 'statusExtern',
    headerName: 'GridFailure.StatusExtern',
    sortable: true,
    suppressMovable: true,
    filter: 'setFilterComponent',
  },
  {
    field: 'branch',
    headerName: 'GridFailure.Branch',
    sortable: true,
    suppressMovable: true,
    filter: 'setFilterComponent',
  },
  {
    field: 'voltageLevel',
    headerName: 'GridFailure.VoltageLevel',
    sortable: true,
    suppressMovable: true,
    filter: 'setFilterComponent',
  },
  {
    field: 'pressureLevel',
    headerName: 'GridFailure.PressureLevel',
    sortable: true,
    suppressMovable: true,
    filter: 'setFilterComponent',
  },
  {
    field: 'failureBegin',
    headerName: 'GridFailure.FailureBegin',
    sortable: true,
    suppressMovable: true,
    filter: 'agDateColumnFilter',
    filterParams: {
      comparator: dateTimeComparator,
      browserDatePicker: true,
    },
    cellRendererFramework: DateTimeCellRendererComponent,
  },
  {
    field: 'failureEndPlanned',
    headerName: 'GridFailure.FailureEndPlanned',
    sortable: true,
    suppressMovable: true,
    filter: 'agDateColumnFilter',
    filterParams: {
      comparator: dateTimeComparator,
      browserDatePicker: true,
    },
    cellRendererFramework: DateTimeCellRendererComponent,
  },
  {
    field: 'failureEndResupplied',
    headerName: 'GridFailure.FailureEndResupplied',
    sortable: true,
    suppressMovable: true,
    filter: 'agDateColumnFilter',
    filterParams: {
      comparator: dateTimeComparator,
      browserDatePicker: true,
    },
    cellRendererFramework: DateTimeCellRendererComponent,
  },
  {
    field: 'expectedReasonText',
    headerName: 'GridFailure.ExpectedReason',
    sortable: true,
    suppressMovable: true,
    filter: 'setFilterComponent',
  },
  {
    field: 'internalRemark',
    headerName: 'GridFailure.InternalRemark',
    sortable: true,
    suppressMovable: true,
    filter: 'setFilterComponent',
  },
  {
    field: 'postcode',
    headerName: 'GridFailure.Postcode',
    sortable: true,
    suppressMovable: true,
    filter: 'setFilterComponent',
    valueGetter: params => (params.data.postcode ? params.data.postcode : params.data.freetextPostcode),
  },
  {
    field: 'city',
    headerName: 'GridFailure.City',
    sortable: true,
    suppressMovable: true,
    filter: 'setFilterComponent',
    valueGetter: params => (params.data.city ? params.data.city : params.data.freetextCity),
  },
  {
    field: 'district',
    headerName: 'GridFailure.District',
    sortable: true,
    suppressMovable: true,
    filter: 'setFilterComponent',
    valueGetter: params => (params.data.district ? params.data.district : params.data.freetextDistrict),
  },
  {
    field: 'street',
    headerName: 'GridFailure.Street',
    sortable: true,
    suppressMovable: true,
    filter: 'setFilterComponent',
  },
  {
    field: 'housenumber',
    headerName: 'GridFailure.Housenumber',
    sortable: true,
    suppressMovable: true,
    filter: 'setFilterComponent',
  },
  {
    field: 'radius',
    headerName: 'GridFailure.RadiusInM',
    sortable: true,
    suppressMovable: true,
    filter: 'setFilterComponent',
  },
  {
    field: 'stationDescription',
    headerName: 'GridFailure.StationDescription',
    sortable: true,
    suppressMovable: true,
    filter: 'setFilterComponent',
  },
  {
    field: 'tools',
    headerName: ' ',
    pinned: 'right',
    maxWidth: 110,
    minWidth: 110,
    lockPosition: true,
    sortable: false,
    filter: false,
    suppressMenu: true,
    suppressSizeToFit: true,
    cellRendererFramework: IconCellRendererComponent,
  },
];
