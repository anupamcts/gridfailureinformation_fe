/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { IconCellRendererComponent } from '@grid-failure-information-app/shared/components/cell-renderer/icon-cell-renderer/icon-cell-renderer.component';

export const DISTRIBUTION_GROUP_COLDEF = [
  {
    field: 'name',
    headerName: 'DistributionGroup.Name',
    sortable: true,
    suppressMovable: true,
    filter: 'setFilterComponent',
    sort: 'asc',
    comparator: (a: string, b: string) => a.localeCompare(b),
  },
  {
    field: 'emailSubjectPublish',
    headerName: 'DistributionGroup.SubjectPublish',
    sortable: true,
    suppressMovable: true,
    filter: 'setFilterComponent',
  },
  {
    field: 'distributionTextPublish',
    headerName: 'DistributionGroup.TextPublish',
    sortable: true,
    suppressMovable: true,
    filter: 'setFilterComponent',
  },
  {
    field: 'emailSubjectUpdate',
    headerName: 'DistributionGroup.SubjectUpdate',
    sortable: true,
    suppressMovable: true,
    filter: 'setFilterComponent',
  },
  {
    field: 'distributionTextUpdate',
    headerName: 'DistributionGroup.TextUpdate',
    sortable: true,
    suppressMovable: true,
    filter: 'setFilterComponent',
  },
  {
    field: 'emailSubjectComplete',
    headerName: 'DistributionGroup.SubjectComplete',
    sortable: true,
    suppressMovable: true,
    filter: 'setFilterComponent',
  },
  {
    field: 'distributionTextComplete',
    headerName: 'DistributionGroup.TextComplete',
    sortable: true,
    suppressMovable: true,
    filter: 'setFilterComponent',
  },
  {
    field: 'tools',
    headerName: ' ',
    pinned: 'right',
    maxWidth: 55,
    minWidth: 55,
    lockPosition: true,
    sortable: false,
    filter: false,
    suppressMenu: true,
    suppressSizeToFit: true,
    cellRendererFramework: IconCellRendererComponent,
  },
];
