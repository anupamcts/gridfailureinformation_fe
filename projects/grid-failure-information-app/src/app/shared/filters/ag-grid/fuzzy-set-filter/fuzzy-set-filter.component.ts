/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component, OnInit } from '@angular/core';
import { IDoesFilterPassParams, IFilterParams, RowDataChangedEvent, RowNode } from 'ag-grid-community';
import { SetFilterComponent } from '@grid-failure-information-app/shared/filters/ag-grid/set-filter/set-filter.component';

@Component({
  selector: 'fuzzy-filter-cell',
  templateUrl: './fuzzy-set-filter.component.html',
  styleUrls: ['./fuzzy-set-filter.component.css'],
})
export class FuzzySetFilterComponent extends SetFilterComponent {
  doesFilterPass(params: IDoesFilterPassParams): boolean {
    const nodeValue = this._valueGetter(params.node);
    let passed: boolean = false;

    for (let itemKey in this.setItems) {
      if (isNaN(nodeValue)) {
        if (itemKey === 'null') {
          passed = !nodeValue || nodeValue === ''; // nodeValue is falsy or empty string
        } else {
          passed = !!nodeValue && nodeValue.includes(itemKey);
        }
      } else {
        // is numeric or null
        if (!nodeValue) {
          passed = (nodeValue === null || nodeValue === '') && itemKey === 'null';
        } else {
          passed = nodeValue.toString() === itemKey;
        }
      }
      passed = passed && this.setItems[itemKey].checked;
      if (passed) return passed;
    }
    return passed;
  }
}
