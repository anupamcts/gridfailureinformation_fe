/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { DateTimeModel } from '@grid-failure-information-app/shared/models/date-time.model';
import { NgbDateCustomParserFormatter } from '@grid-failure-information-app/shared/pipes/ngb-date-custom-parser-formatter';

describe('NgbDateCustomParserFormatter', () => {
  let service: NgbDateCustomParserFormatter;

  beforeEach(() => {
    service = new NgbDateCustomParserFormatter();
  });

  it('should create NgbDateCustomParserFormatter service', () => {
    expect(service).toBeTruthy();
  });

  it('should format a date to string', () => {
    const date: DateTimeModel = new DateTimeModel({
      year: 2020,
      month: 4,
      day: 1,
      hour: 12,
      minute: 30,
      second: 30,
    });

    expect(service.format(date)).toBe('01.04.2020 / 00:00');
  });

  it('should parse a string with 1 part into a date', () => {
    const stringDate: string = '01';
    expect(service.parse(stringDate)).toEqual(Object({ day: 1, month: null, year: null, hour: null, minute: null, second: null }));
  });

  it('should parse a string with 2 parts into a date', () => {
    const stringDate: string = '01/04';
    expect(service.parse(stringDate)).toEqual(Object({ day: 1, month: 4, year: null, hour: null, minute: null, second: null }));
  });

  it('should parse a string with 3 parts into a date', () => {
    const stringDate: string = '01/04/2020';
    expect(service.parse(stringDate)).toEqual(Object({ day: 1, month: 4, year: 2020, hour: null, minute: null, second: null }));
  });

  it('should parse a string with 4 parts into a date', () => {
    const stringDate: string = '01/04/2020/12';
    expect(service.parse(stringDate)).toEqual(Object({ day: 1, month: 4, year: 2020, hour: 12, minute: null, second: null }));
  });

  it('should parse a string with 5 parts into a date', () => {
    const stringDate: string = '01/04/2020/12/30';
    expect(service.parse(stringDate)).toEqual(Object({ day: 1, month: 4, year: 2020, hour: 12, minute: 30, second: null }));
  });

  it('should parse a string with 6 parts into a date', () => {
    const stringDate: string = '01/04/2020/12/30/30';
    expect(service.parse(stringDate)).toEqual(Object({ day: 1, month: 4, year: 2020, hour: 12, minute: 30, second: 30 }));
  });

  it('should return null parse called without value', () => {
    expect(service.parse(null)).toEqual(null);
  });
});
