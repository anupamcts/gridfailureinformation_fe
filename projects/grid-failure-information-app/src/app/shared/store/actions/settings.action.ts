/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { createAction, props } from '@ngrx/store';
import { User } from '@grid-failure-information-app/shared/models/user';
import { Settings } from '@grid-failure-information-app/shared/models';
import { InitialEmailContent } from '@grid-failure-information-app/shared/models/settings-initial-email-content.model';

interface ISetLanguageProps {
  payload: string;
}
interface ISetCultureProps {
  payload: string;
}
interface ISetUserProps {
  payload: User;
}
export interface ILoadPreConfigurationSuccess {
  payload: Settings;
}
export interface ILoadPreConfigurationFail {
  payload: string;
}

export interface ILoadInitialEmailContentSuccess {
  payload: InitialEmailContent;
}
export interface ILoadInitialEmailContentFail {
  payload: string;
}

export const setLanguage = createAction('[Settings] SetLanguage', props<ISetLanguageProps>());

export const setCulture = createAction('[Settings] SetCulture', props<ISetCultureProps>());

export const setUser = createAction('[Settings] SetUser', props<ISetUserProps>());

export const loadPreConfiguration = createAction('[Settings PreConfiguration] Load');
export const loadPreConfigurationSuccess = createAction('[Settings PreConfiguration] Load Success', props<ILoadPreConfigurationSuccess>());
export const loadPreConfigurationFail = createAction('[Settings PreConfiguration] Load Fail', props<ILoadPreConfigurationFail>());

export const loadInitialEmailContent = createAction('[Settings InitialEmailContent] Load');
export const loadInitialEmailContentSuccess = createAction('[Settings InitialEmailContent] Load Success', props<ILoadInitialEmailContentSuccess>());
export const loadInitialEmailContentFail = createAction('[Settings InitialEmailContent] Load Fail', props<ILoadInitialEmailContentFail>());
