/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import {
  settingsReducer,
  INITIAL_STATE,
  reducer,
  getSelectedLanguage,
  getSelectedCulture,
  getAvailableLanguages,
  getUser,
  getPreConfiguration,
  getExportChannels,
} from '@grid-failure-information-app/shared/store/reducers/settings.reducer';
import * as settingsActions from '@grid-failure-information-app/shared/store/actions/settings.action';
import { User } from '@grid-failure-information-app/shared/models/user';
import { Settings } from '@grid-failure-information-app/shared/models';

describe('Settings reducer', () => {
  it('should return the initial state', () => {
    const action = { type: 'NOOP' } as any;
    const result = reducer(undefined, action);

    expect(result).toBe(INITIAL_STATE);
  });

  it('should set language', () => {
    const action = settingsActions.setLanguage({ payload: 'de' });
    const result = settingsReducer(INITIAL_STATE, action);

    expect(result).toEqual({
      ...INITIAL_STATE,
      selectedLanguage: 'de',
    });
  });

  it('should set culture', () => {
    const action = settingsActions.setCulture({ payload: 'DE-de' });
    const result = settingsReducer(INITIAL_STATE, action);

    expect(result).toEqual({
      ...INITIAL_STATE,
      selectedCulture: 'DE-de',
    });
  });

  it('should set User', () => {
    const user = new User();
    user.name = 'Max Muster';
    const action = settingsActions.setUser({ payload: user });
    const result = settingsReducer(INITIAL_STATE, action);

    expect(result).toEqual({
      ...INITIAL_STATE,
      user: user,
    });
  });

  it('should return loading state for loadPreConfiguration', () => {
    const action = settingsActions.loadPreConfiguration();
    const result = settingsReducer(INITIAL_STATE, action);

    expect(result.preConfiguration.loading).toBeTruthy();
  });

  it('should return loaded state for loadPreConfigurationSuccess', () => {
    const settings = new Settings();
    const action = settingsActions.loadPreConfigurationSuccess({ payload: settings });
    const result = settingsReducer(INITIAL_STATE, action);

    expect(result.preConfiguration.loaded).toBeTruthy();
  });

  it('should return failed state for loadPreConfigurationFail', () => {
    const action = settingsActions.loadPreConfigurationFail({ payload: 'error' });
    const result = settingsReducer(INITIAL_STATE, action);

    expect(result.preConfiguration.failed).toBeTruthy();
  });

  it('getSelectedLanguage should return state.selectedLanguage', () => {
    const state = { ...INITIAL_STATE };
    const result = getSelectedLanguage(state);
    expect(result).toBe(state.selectedLanguage);
  });

  it('getSelectedCulture should return state.selectedCulture', () => {
    const state = { ...INITIAL_STATE };
    const result = getSelectedCulture(state);
    expect(result).toBe(state.selectedCulture);
  });

  it('getAvailableLanguages should return state.availableLanguages', () => {
    const state = { ...INITIAL_STATE };
    const result = getAvailableLanguages(state);
    expect(result).toBe(state.availableLanguages);
  });

  it('getUser should return state.getUser', () => {
    const state = { ...INITIAL_STATE };
    const result = getUser(state);
    expect(result).toBe(state.user);
  });

  it('getPreConfiguration should return state.getPreConfiguration.data', () => {
    const state = { ...INITIAL_STATE };
    const result = getPreConfiguration(state);
    expect(result).toBe(state.preConfiguration.data);
  });
});
