/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Action, ActionReducer, INIT } from '@ngrx/store';
import * as distributionGroupActions from '@grid-failure-information-app/shared/store/actions/distribution-groups.action';
import { createFormGroupState, createFormStateReducerWithUpdate, updateGroup, FormGroupState, FormState, SetValueAction, validate } from 'ngrx-forms';
import { DistributionGroup } from '@grid-failure-information-app/shared/models';
import { required } from 'ngrx-forms/validation';

export const FORM_ID = 'distributionGroupDetailForm';

export const INITIAL_STATE: FormGroupState<DistributionGroup> = createFormGroupState<DistributionGroup>(FORM_ID, new DistributionGroup());

export const validateForm: ActionReducer<FormState<DistributionGroup>> = createFormStateReducerWithUpdate<DistributionGroup>(
  updateGroup<DistributionGroup>({
    name: validate(required),
    emailSubjectView: validate(required),
    emailSubjectPublish: validate(required),
    emailSubjectComplete: validate(required),
    emailSubjectUpdate: validate(required),
  })
);

export function reducer(state: FormGroupState<DistributionGroup> = INITIAL_STATE, action: Action): FormGroupState<DistributionGroup> {
  if (!action || action.type === INIT) {
    return INITIAL_STATE;
  }
  if (action.type === distributionGroupActions.loadDistributionGroupsDetailSuccess.type) {
    const item: DistributionGroup = <DistributionGroup>action['payload'];
    const setValueAction: SetValueAction<any> = new SetValueAction<any>(FORM_ID, item);

    return validateForm(state, setValueAction);
  }

  return validateForm(state, action);
}
export const getFormState = (state: FormGroupState<DistributionGroup>) => state;
