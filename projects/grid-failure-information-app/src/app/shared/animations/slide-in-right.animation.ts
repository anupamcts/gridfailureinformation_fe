 /********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { AnimationTriggerMetadata } from '@angular/animations';
import {
  animate,
  state,
  style,
  transition,
  trigger
} from '@angular/animations';

// Component transition animations
export const slideInRightAnimation: AnimationTriggerMetadata = trigger(
  'slideInRightAnimation',
  [
    state('in', style({ opacity: 1, transform: 'translateX(0)' })),
    transition('void => *', [
      style({
        opacity: 0,
        transform: 'translateX(100%)'
      }),
      animate('0.2s ease-in')
    ]),
    transition('* => void', [
      animate(
        '0.2s 10 ease-out',
        style({
          opacity: 0,
          transform: 'translateX(100%)'
        })
      )
    ])
  ]
);
