/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { BoolCellRendererComponent } from '@grid-failure-information-app/shared/components/cell-renderer/bool-cell-renderer/bool-cell-renderer.component';
import { DateCellRendererComponent } from '@grid-failure-information-app/shared/components/cell-renderer/date-cell-renderer/date-cell-renderer.component';
import { DateTimeCellRendererComponent } from '@grid-failure-information-app/shared/components/cell-renderer/date-time-cell-renderer/date-time-cell-renderer.component';
import { HeaderCellRendererComponent } from '@grid-failure-information-app/shared/components/cell-renderer/header-cell-renderer/header-cell-renderer.component';
import { IconCellRendererComponent } from '@grid-failure-information-app/shared/components/cell-renderer/icon-cell-renderer/icon-cell-renderer.component';
import { DateTimePickerComponent } from '@grid-failure-information-app/shared/components/date-time-picker/date-time-picker.component';
import { SafetyQueryDialogComponent } from '@grid-failure-information-app/shared/components/dialogs/safety-query-dialog/safety-query-dialog.component';
import { ExpandableComponent } from '@grid-failure-information-app/shared/components/expandable/expandable.component';
import { HeaderComponent } from '@grid-failure-information-app/shared/components/header/header.component';
import { LoadingPlaceholderComponent } from '@grid-failure-information-app/shared/components/loading-placeholder/loading-placeholder.component';
import { LoadingSpinnerComponent } from '@grid-failure-information-app/shared/components/loading-spinner/loading-spinner.component';
import { PageNotFoundComponent } from '@grid-failure-information-app/shared/components/page-not-found/pageNotFound.component';
import { PaginationComponent } from '@grid-failure-information-app/shared/components/pagination/pagination.component';
import { SpinnerComponent } from '@grid-failure-information-app/shared/components/spinner/spinner.component';
import { VersionInfo } from '@grid-failure-information-app/shared/components/version-info/version-info.component';
import { DirectivesModule } from '@grid-failure-information-app/shared/directives/directives.module';
import { PipesModule } from '@grid-failure-information-app/shared/pipes/pipes.module';
import { NgbDatepickerModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { AgGridModule } from 'ag-grid-angular';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { NgrxFormsModule } from 'ngrx-forms';

export const COMPONENTS = [
  SpinnerComponent,
  HeaderComponent,
  PageNotFoundComponent,
  LoadingPlaceholderComponent,
  VersionInfo,
  LoadingSpinnerComponent,
  PaginationComponent,
  IconCellRendererComponent,
  SafetyQueryDialogComponent,
  ExpandableComponent,
  SafetyQueryDialogComponent,
  BoolCellRendererComponent,
  DateCellRendererComponent,
  HeaderCellRendererComponent,
  DateTimePickerComponent,
  DateTimeCellRendererComponent,
];

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    DirectivesModule,
    ReactiveFormsModule,
    RouterModule,
    NgrxFormsModule,
    FormsModule,
    AngularFontAwesomeModule,
    NgbDatepickerModule,
    NgbModule,

    AgGridModule.withComponents([]),

    PipesModule,
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS,
  entryComponents: [
    PaginationComponent,
    IconCellRendererComponent,
    SafetyQueryDialogComponent,
    BoolCellRendererComponent,
    DateCellRendererComponent,
    HeaderCellRendererComponent,
    DateTimeCellRendererComponent,
  ],
  providers: [DatePipe],
})
export class ComponentsModule {}
