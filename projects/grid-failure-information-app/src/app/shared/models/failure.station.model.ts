/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
export class FailureStation {
  public g3efid: number = null;
  public id: string = null;
  public longitude: number = null;
  public latitude: number = null;
  public sdoxl: number = null;
  public sdoyl: number = null;
  public stationId: string = null;
  public stationName: string = null;

  public constructor(data: any = null) {
    Object.keys(data || {})
      .filter(property => this.hasOwnProperty(property))
      .forEach(property => (this[property] = data[property]));
  }

  get failureStationSearchString(): string {
    return this.stationName + ' (' + this.stationId + ')';
  }
}
