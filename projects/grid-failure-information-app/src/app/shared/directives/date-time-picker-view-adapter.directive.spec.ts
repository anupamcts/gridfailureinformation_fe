/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { DateTimePickerViewAdapterDirective } from '@grid-failure-information-app/shared/directives/date-time-picker-view-adapter.directive';
import { DateTimePickerComponent } from '@grid-failure-information-app/shared/components/date-time-picker/date-time-picker.component';
import { NgbPopoverConfig } from '@ng-bootstrap/ng-bootstrap';
import { DateTimeModel } from '@grid-failure-information-app/shared/models/date-time.model';
import { dateTimePickerValueConverter } from '@grid-failure-information-app/shared/utility';
import { FormControlState } from 'ngrx-forms';

describe('DateTimePickerViewAdapaterDirective', () => {
  let config: NgbPopoverConfig;
  let component: DateTimePickerComponent;
  let directive: DateTimePickerViewAdapterDirective;

  beforeEach(() => {
    config = new NgbPopoverConfig();
    component = new DateTimePickerComponent(config);
    directive = new DateTimePickerViewAdapterDirective(component);
  });

  it('should create an instance', () => {
    expect(directive).toBeTruthy();
  });

  it('should set dateTimeViewValue to null', () => {
    directive.setViewValue(null);
    expect(directive['_dateTimePickerComponent'].dateTimeViewValue).toBeNull;
  });

  it('should set dateTimeViewValue to model', () => {
    directive.setViewValue('20200618');
    directive['dateTimeModel'] = DateTimeModel.fromLocalString('20200618');
    expect(directive['_dateTimePickerComponent'].dateTimeViewValue).toBe(dateTimePickerValueConverter.convertStateToViewValue(directive['dateTimeModel']));
  });

  it('should set onChangeCallback', () => {
    let onChangeCallback = value => {};
    directive.setOnChangeCallback(onChangeCallback);
    expect(directive['_dateTimePickerComponent'].onValueChangeCallBack).toBeDefined();
  });

  it('should set markTouchedCallBack', () => {
    directive.setOnTouchedCallback(() => {});
    expect(directive['_dateTimePickerComponent'].markTouched).toBeDefined();
  });

  it('should set isDisabled', () => {
    directive.setIsDisabled(true);
    expect(directive['_dateTimePickerComponent'].isDisabled).toBeTruthy();
  });

  it('should set isValid property', () => {
    let state: FormControlState<any> = { isValid: true } as any;
    directive.ngrxFormControlState = state;
    expect(directive['_dateTimePickerComponent'].isValid).toBeTruthy();
  });
});
