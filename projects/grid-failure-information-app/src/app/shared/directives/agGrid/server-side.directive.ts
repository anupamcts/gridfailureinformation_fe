/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { PageEvent } from '@grid-failure-information-app/shared/models/page-event';
import { PageModel } from '@grid-failure-information-app/shared//models/page/page.model';
import { AgGridAngular } from 'ag-grid-angular';
import { map } from 'rxjs/operators';
import { ServerSideModel } from '@grid-failure-information-app/shared/models/server-side.model';
import { Directive, Input, ComponentFactoryResolver, ViewContainerRef, ComponentFactory, OnInit } from '@angular/core';
import { Store, ActionsSubject } from '@ngrx/store';

import * as store from '@grid-failure-information-app/shared/store';
import { PageRequestInterface } from '@grid-failure-information-app/shared/models/page/page-request.interface';
import { ofType } from '@ngrx/effects';
import { PaginationComponent } from '@grid-failure-information-app/shared/components/pagination/pagination.component';

/**
 * Realizes server side paging
 *
 * @author Martin Gardyan <martin.gardyan@pta.de>
 * @export
 * @class ServerSideDirective
 * @implements {OnInit}
 */
@Directive({
  selector: 'ag-grid-angular[serverSide]',
  host: {},
})
export class ServerSideDirective implements OnInit {
  @Input()
  public serverSide: ServerSideModel;

  @Input()
  public set queryParameter(query: any) {
    if (!!query) {
      this._appState$.dispatch(
        this.serverSide.loadAction({
          payload: {
            queryParameter: query,
            pageSize: this.serverSide.pageSize,
          } as PageRequestInterface,
        })
      );
    }
    this._queryParameter = query;
  }
  private _queryParameter: any;

  private _matPagination: PaginationComponent;

  /**
   * @author Martin Gardyan <martin.gardyan@pta.de>
   * @param {Store<store.State>} _appState$
   * @param {ComponentFactoryResolver} _resolver
   * @param {ViewContainerRef} _viewContainerRef
   * @param {ActionsSubject} _actionsSubject
   * @param {AgGridAngular} _agGrid
   * @memberof ServerSideDirective
   */
  constructor(
    private _appState$: Store<store.State>,
    private _resolver: ComponentFactoryResolver,
    private _viewContainerRef: ViewContainerRef,
    private _actionsSubject: ActionsSubject,
    private _agGrid: AgGridAngular
  ) {}

  /**
   * @author Martin Gardyan <martin.gardyan@pta.de>
   * @memberof ServerSideDirective
   */
  ngOnInit() {
    this._actionsSubject
      .pipe(
        ofType(this.serverSide.successAction.type),
        map(item => item.payload)
      )
      .subscribe((pagedItem: PageModel<any>) => {
        if (!this._matPagination) {
          const pagination: ComponentFactory<PaginationComponent> = this._resolver.resolveComponentFactory(PaginationComponent);
          this._matPagination = this._viewContainerRef.createComponent(pagination).instance;
        }
        if (this._matPagination.totalPages !== pagedItem.totalPages) {
          this._matPagination.totalPages = pagedItem.totalPages;
        }

        this._matPagination.length = pagedItem.totalElements;
        this._matPagination.pageSize = pagedItem.pageable.pageSize || 1;
        this._matPagination.hidePageSize = false;

        this._matPagination.page.subscribe((selectedPage: PageEvent) => {
          this._retrievePage(selectedPage);
        });

        if (!!this._agGrid.api) {
          this._agGrid.api.setRowData(pagedItem.content);
        } else {
          this._agGrid.rowData = pagedItem.content;
        }
      });

    this._retrievePage();
  }

  /**
   * @author Martin Gardyan <martin.gardyan@pta.de>
   * @private
   * @param {PageEvent} [event]
   * @memberof ServerSideDirective
   */
  private _retrievePage(event?: PageEvent) {
    this._appState$.dispatch(
      this.serverSide.loadAction({
        payload: {
          pageNumber: (event && event.pageIndex) || 0,
          pageSize: this.serverSide.pageSize,
          queryParameter: this._queryParameter,
        } as PageRequestInterface,
      })
    );
  }
}
