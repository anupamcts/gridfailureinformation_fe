/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async } from '@angular/core/testing';
import { ConfigService } from '@grid-failure-information-app/app/app-config.service';
import { of } from 'rxjs/observable/of';

describe('ConfigService', () => {
  let component: ConfigService;
  let http: any;
  let baseHref: any;

  beforeEach(async(() => {
    http = {
      get: () =>
        of({
          json: () => of({ env: 'env_json' }),
        }),
    } as any;
  }));

  beforeEach(() => {
    baseHref = '/test';
    component = new ConfigService(http, baseHref);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get env', () => {
    const keyValue = 'test';
    (component as any).env = { key: keyValue };
    const key: any = 'key';
    const res = component.getEnv(key);
    expect(res).toBe(keyValue);
  });

  it('should get config key', () => {
    const keyValue = 'test';
    (component as any).config = { key: keyValue };
    const key: any = 'key';
    const res = component.get(key);
    expect(res).toBe(keyValue);
  });

  it('should load the config file', () => {
    const keyValue = 'test';
    (component as any).config = { key: keyValue };
    (component as any).env = { key: keyValue };
    component.load();
    expect((component as any).config).toBeDefined();
  });

  it('should transform config response', () => {
    const response: any = { exportChannels: ['X'] };
    const item = ConfigService.configAdapter(response);
    expect(item.exportChannels).toBe(response.exportChannels);
  });
});
