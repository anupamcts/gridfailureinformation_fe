/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import * as utilityHelpers from '@openk-libs/grid-failure-information-map/shared/utility/utilityHelpers';
import { VisibilityConfigurationInterface } from '@openk-libs/grid-failure-information-map/shared/models/visibility-configuration.interface';

describe('utilityHelpers', () => {
  it('should parse string to Integer via toInteger()', () => {
    let testValue = utilityHelpers.toInteger('1');
    expect(testValue).toBe(parseInt(`${1}`, 10));
  });

  it('should return true if a value is a number', () => {
    let testValue = utilityHelpers.isNumber('1');
    expect(testValue).toBe(true);
  });

  it('should return false if a value is not a number', () => {
    let testValue = utilityHelpers.isNumber('a');
    expect(testValue).toBe(false);
  });

  it('should pad the number if a value is a number', () => {
    let testValue = utilityHelpers.padNumber(1);
    expect(testValue).toBe('01');
  });

  it('should return empty string if a value is not a number', () => {
    let testValue = utilityHelpers.padNumber(null);
    expect(testValue).toBe('');
  });

  it('should convert ISO time to local time', () => {
    const dateString: string = '2020-11-19T14:13:15.666Z';
    let testValue = utilityHelpers.convertISOToLocalDateTime(dateString);
    expect(testValue).toBe('19.11.2020 / 14:13');
  });

  it('should return true for detailFieldVisibility in case no config is provided', () => {
    let config: VisibilityConfigurationInterface = null;
    let testValue = utilityHelpers.determineDetailFieldVisibility(config, 'fieldVisibility', 'description');
    expect(testValue).toBeTruthy();
  });
});
