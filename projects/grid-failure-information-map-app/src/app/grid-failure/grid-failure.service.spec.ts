/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { GridFailureService } from '@grid-failure-information-map-app/app/grid-failure/grid-failure.service';

describe('GridFailureService', () => {
  let service: GridFailureService;
  let mockHttpClient: any = null;

  beforeEach(() => {
    mockHttpClient = { get: () => {} };
    service = new GridFailureService(mockHttpClient);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get data via getGridFailureData()', () => {
    const spy: any = spyOn(service['_http'], 'get');
    service.getGridFailureData();
    expect(spy).toHaveBeenCalled();
  });
});
